import React, { useState } from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';
import RadioForm from 'react-native-simple-radio-button';
import {Picker} from '@react-native-picker/picker';

export default function App() {
  const [weight, setWeight] = useState('');
  const [bottles, setBottles] = useState(1);
  const [time, setTime] = useState(1);
  const [gender, setGender] = useState('male');
  const [promilles, setPromilles] = useState(0);

  const items = [
    { label: '1', value: 1 },
    { label: '2', value: 2 },
    { label: '3', value: 3 },
    { label: '4', value: 4 },
    { label: '5', value: 5 },
    { label: '6', value: 6 },
    { label: '7', value: 7 },
    { label: '8', value: 8 },
    { label: '9', value: 9 },
    { label: '10', value: 10 },
    { label: '11', value: 11 },
    { label: '12', value: 12 },
    { label: '13', value: 13 },
    { label: '14', value: 14 },
    { label: '15', value: 15 },
    { label: '16', value: 16 },
    { label: '17', value: 17 },
    { label: '18', value: 18 },
    { label: '19', value: 19 },
    { label: '20', value: 20 },
    { label: '21', value: 21 },
    { label: '22', value: 22 },
    { label: '23', value: 23 },
    { label: '24', value: 24 },
  ];

  function calculate() {
    let result = 0;
    let liters = bottles * 0.33;
    let grams = liters * 8 * 4.5;
    let burning = weight / 10;
    let gramsleft = grams - burning * time;

    if (gender === 'male') {
      result = gramsleft / (weight * 0.7);
    } else {
      result = gramsleft / (weight * 0.6);
    }

    if (result < 0 || result == Infinity) {
      result = 0;
    } 

    setPromilles(result);
  }

  return (
    <View style={styles.container}>
      <View style={styles.field}>
        <Text>Weight (kg)</Text>
        <TextInput
          style={styles.input}
          value={weight}
          keyboardType="number-pad"
          placeholder="Enter weight in kilograms."
          onChangeText={text => setWeight(text)}></TextInput>
      </View>

      <View style={[styles.field, {zIndex: 6000}]}>
      <Text>Bottles (amount)</Text>
        <Picker 
        labelStyle={{ color: '#000' }}
        containerStyle={{ height: 40 }}
        items={items}
        selectedValue={bottles}
        onValueChange={(itemValue) => setBottles(itemValue)}>
          {items.map((bottles,index) => (
            <Picker.Item key={index} label={bottles.label} value={bottles.value}/>
          ))
          }
        </Picker>
      </View>

      <View style={[styles.field, {zIndex: 5000}]}>
      <Text>Time (hours)</Text>
        <Picker 
        labelStyle={{ color: '#000' }}
        containerStyle={{ height: 40 }}
        items={items}
        selectedValue={time}
        onValueChange={(itemValue) => setTime(itemValue)}>
          {items.map((time,index) => (
            <Picker.Item key={index} label={time.label} value={time.value}/>
          ))
          }
        </Picker>
      </View>

      <View style={styles.field}>
        <Text>Gender</Text>
        <RadioForm
          radio_props={[
            { label: 'Male', value: 'male' },
            { label: 'Female', value: 'female' }
          ]}
          onPress={(value) => { setGender(value) }}
        >
        </RadioForm>
      </View>

      <View style={styles.field}>
        <Text>Promilles</Text>
        <Text>{promilles.toFixed(2)}</Text>
      </View>

      <View style={styles.field}>
        <Button onPress={calculate} title="calculate" />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: 50,
    marginRight: 20,
  },
  input: {
    marginTop: 10,
  },
  field: {
    margin: 10,
  }
});
